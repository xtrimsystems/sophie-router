import { AbstractResource } from './AbstractResource';

export interface ResourceRouter
{
	route (pathName: string): AbstractResource;
	setNext (router: ResourceRouter): void;
}
