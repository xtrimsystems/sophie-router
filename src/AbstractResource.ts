import { AbstractApplication } from './Framework/index';

export abstract class AbstractResource
{
	public abstract run (application: AbstractApplication, pathName?: string, search?: string): void;

	public abstract getPath (): RegExp;

	public isIdentifiedBy (pathName: string): boolean
	{
		return !!pathName.match(this.getPath());
	}
}
