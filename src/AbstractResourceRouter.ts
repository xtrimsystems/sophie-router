import { AbstractResource } from './AbstractResource';
import { NoMoreRoutersException } from './NoMoreRoutersException';
import { ResourceRouter } from './ResourceRouter';

export abstract class AbstractResourceRouter implements ResourceRouter
{
	private next: ResourceRouter;
	private readonly routes: AbstractResource[] = [];

	public addResource (resource: AbstractResource)
	{
		this.routes.push(resource);
	}

	public route (pathName: string): AbstractResource
	{
		if (this.canRoute(pathName))
		{
			return this.doRoute(pathName);
		}

		if (this.next !== undefined)
		{
			return this.next.route(pathName);
		}

		throw new NoMoreRoutersException();
	}

	public setNext (router: ResourceRouter): void
	{
		this.next = router;
	}

	protected abstract canRoute (pathName: string): boolean;

	private doRoute (pathName: string): AbstractResource
	{
		for (const resource of this.routes)
		{
			if (resource.isIdentifiedBy(pathName))
			{
				return resource;
			}
		}
		throw new NoMoreRoutersException();
	}
}
