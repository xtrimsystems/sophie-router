import { AbstractResource } from './AbstractResource';
import { NoMoreRoutersException } from './NoMoreRoutersException';
import { ResourceRouter } from './ResourceRouter';

export class RouterChain
{
	private first: ResourceRouter;
	private last: ResourceRouter;

	public addRouter (router: ResourceRouter)
	{
		if (this.first ===  undefined) {
			this.first = router;
		}
		if (this.last !== undefined) {
			this.last.setNext(router);
		}
		// TODO Check this logic of this.last.setNext(router) and later doing this.last = router
		this.last = router;
	}

	public route (pathName: string): AbstractResource
	{
		if (this.first === undefined)
		{
			throw new NoMoreRoutersException();
		}

		return this.first.route(pathName);
	}
}
