import { AbstractResource, NoMoreRoutersException, ResourceRouter, RouterChain } from './../';
import { AbstractApplication } from './AbstractApplication';

export class Framework
{
	private readonly routerChain: RouterChain;
	private readonly application: AbstractApplication;

	public static createInstance (application: AbstractApplication): Framework
	{
		return new Framework(new RouterChain(), application);
	}

	public constructor (routerChain: RouterChain, application: AbstractApplication)
	{
		this.routerChain = routerChain;
		this.application = application;
	}

	public registerResourceRouter (router: ResourceRouter): void
	{
		this.routerChain.addRouter(router);
	}

	public run (pathName: string, search?: string): void
	{
		try {
			const resource: AbstractResource = this.routerChain.route(pathName);
			resource.run(this.application, pathName, search);
		} catch (error) {
			throw new NoMoreRoutersException(error);
		}
	}
}
