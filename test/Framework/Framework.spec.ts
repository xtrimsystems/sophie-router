import { instance, mock, verify, when } from "ts-mockito";

import {
	AbstractApplication,
	AbstractResourceRouter,
	Framework,
	NoMoreRoutersException,
	RouterChain,
} from '../../src';

import { AbstractResourceFixture } from "../fixtures/AbstractResourceFixture";

describe('Test Framework class', () => {

	let mockAbstractApplication: AbstractApplication;
	let mockRouterChain: RouterChain;

	beforeEach(() => {
		mockAbstractApplication = mock(AbstractApplication);
		mockRouterChain = mock(RouterChain);
	});

	describe('the static method "createInstance"', () => {
		it('should create a new instance of Framework', () => {
			expect(Framework.createInstance(mockAbstractApplication)).toBeInstanceOf(Framework);
		});
	});

	describe('the method "registerResourceRouter"', () => {
		it('should add a new "ResourceRouter" to the routerChain', () => {

			const mockAbstractResourceRouter = mock(AbstractResourceRouter);

			when(mockRouterChain.addRouter(mockAbstractResourceRouter));
			const routerChain = instance(mockRouterChain);

			const framework = new Framework(routerChain, mockAbstractApplication);

			framework.registerResourceRouter(mockAbstractResourceRouter);

			verify(mockRouterChain.addRouter(mockAbstractResourceRouter)).once();
		});
	});

	describe('the method "run"', () => {

		const PATH: string = '/';

		it('should call method "run" from a "AbstractResource" if it can handle the path', () => {

			const mockAbstractResource = mock(AbstractResourceFixture);
			when(mockAbstractResource.run(mockAbstractApplication, PATH, undefined)).thenReturn();
			const abstractResource = instance(mockAbstractResource);

			when(mockRouterChain.route(PATH)).thenReturn(abstractResource);
			const routerChain = instance(mockRouterChain);

			const framework = new Framework(routerChain, mockAbstractApplication);

			framework.run(PATH, undefined);

			verify(mockRouterChain.route(PATH)).once();
			verify(mockAbstractResource.run(mockAbstractApplication, PATH, undefined)).once();

		});

		it('should throw an exception if there is not an "AbstractResource"', () => {

			when(mockRouterChain.route(PATH)).thenThrow(new NoMoreRoutersException());
			const routerChain = instance(mockRouterChain);

			const framework = new Framework(routerChain, mockAbstractApplication);

			expect(() => framework.run(PATH, undefined)).toThrow();

		});
	});
});
