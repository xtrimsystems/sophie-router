import { instance, mock, verify, when } from 'ts-mockito';

import { AbstractResource, AbstractResourceRouter } from '../src';

import { AbstractResourceFixture } from './fixtures/AbstractResourceFixture';
import { CanNotRouteResourceRouterFixture } from './fixtures/CanNotRouteResourceRouterFixture';
import { CanRouteResourceRouterFixture } from './fixtures/CanRouteResourceRouterFixture';

describe('Test AbstractResourceRouter class', () => {

	const PATH: string = '/';

	it('the method "addResource" should add a new Resource', () => {

		const resourceRouter: AbstractResourceRouter = new CanNotRouteResourceRouterFixture();
		const resourceMock: AbstractResource = mock(AbstractResource);

		resourceRouter.addResource(resourceMock);

		expect((resourceRouter as any).routes.length).toBe(1);
	});

	it('the method "setNext" should set the next ResourceRouter', () => {

		const resourceRouter: AbstractResourceRouter = new CanNotRouteResourceRouterFixture();
		const resourceRouterMock: AbstractResourceRouter = mock(AbstractResourceRouter);

		resourceRouter.setNext(resourceRouterMock);

		expect((resourceRouter as any).next).toBe(resourceRouterMock);
	});

	describe('the method "route"', () => {

		it('should throw an error if it can not route', () => {

			const resourceRouter: AbstractResourceRouter = new CanNotRouteResourceRouterFixture();

			expect(() => resourceRouter.route(PATH)).toThrow();
		});

		it('should throw an error if it can route but there is no "AbstractResource"', () => {

			const resourceRouter: AbstractResourceRouter = new CanRouteResourceRouterFixture();

			expect(() => resourceRouter.route(PATH)).toThrow();
		});

		it('should throw an error if it can route but the "AbstractResource" does not match', () => {

			const resourceRouter: AbstractResourceRouter = new CanNotRouteResourceRouterFixture();
			const resourceMock: AbstractResource = mock(AbstractResourceFixture);
			when(resourceMock.isIdentifiedBy(PATH)).thenReturn(false);

			const resourceMockInstance: AbstractResource = instance(resourceMock);

			resourceRouter.addResource(resourceMockInstance);

			expect(() => resourceRouter.route(PATH)).toThrow();
		});

		it('should return the matching "AbstractResource" if it can route', () => {

			const resourceRouter: AbstractResourceRouter = new CanRouteResourceRouterFixture();
			const resourceMock: AbstractResource  = mock(AbstractResourceFixture);
			when(resourceMock.isIdentifiedBy(PATH)).thenReturn(true);

			const resourceMockInstance: AbstractResource = instance(resourceMock);

			resourceRouter.addResource(resourceMockInstance);

			expect(resourceRouter.route(PATH)).toBe(resourceMockInstance);
		});

		it('should return the next "AbstractResource" if it can not route and there is a next "ResourceRouter" that can route', () => {

			const resourceRouter: AbstractResourceRouter = new CanNotRouteResourceRouterFixture();
			const resourceRouter2: AbstractResourceRouter = mock(CanRouteResourceRouterFixture);
			const mockAbstractResource = mock(AbstractResource);

			when(resourceRouter2.route(PATH)).thenReturn(mockAbstractResource);

			resourceRouter.setNext(instance(resourceRouter2));

			expect(resourceRouter.route(PATH)).toBe(mockAbstractResource);
			verify(resourceRouter2.route(PATH)).once();
		});

		it('should return the second "AbstractResource"', () => {

			const resourceRouter: AbstractResourceRouter = new CanRouteResourceRouterFixture();
			const mockAbstractResource: AbstractResource = mock(AbstractResource);
			const mockAbstractResource2: AbstractResource = mock(AbstractResource);
			when(mockAbstractResource.isIdentifiedBy(PATH)).thenReturn(false);
			when(mockAbstractResource2.isIdentifiedBy(PATH)).thenReturn(true);

			const abstractResource = instance(mockAbstractResource);
			const abstractResource2 = instance(mockAbstractResource2);

			resourceRouter.addResource(abstractResource);
			resourceRouter.addResource(abstractResource2);

			expect(resourceRouter.route(PATH)).toBe(abstractResource2);
			verify(mockAbstractResource.isIdentifiedBy(PATH)).once();
			verify(mockAbstractResource2.isIdentifiedBy(PATH)).once();
		});
	});
});
