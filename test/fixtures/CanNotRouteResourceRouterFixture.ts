import { AbstractResourceRouter } from '../../src';

export class CanNotRouteResourceRouterFixture extends AbstractResourceRouter
{
	protected canRoute (pathName: string): boolean
	{
		return false;
	}
}
