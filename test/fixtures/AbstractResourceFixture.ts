import { AbstractApplication, AbstractResource } from '../../src';

export class AbstractResourceFixture extends AbstractResource
{
	public getPath (): RegExp {
		return /^\/+$/g;
	}

	public run (abstractApplication: AbstractApplication, pathName: string, search?: string): void {}
}
