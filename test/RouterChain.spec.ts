import { instance, mock, verify, when } from 'ts-mockito';

import { AbstractResourceRouter, RouterChain } from '../src';

describe('Test RouterChain class', () => {

	let routerChain: RouterChain;
	let routerMock: AbstractResourceRouter;
	let router2Mock: AbstractResourceRouter;
	const PATH = '/';

	beforeEach(() => {
		routerChain = new RouterChain();
		routerMock = mock(AbstractResourceRouter);
		router2Mock = mock(AbstractResourceRouter);
	});

	describe('the method "addRouter"', () => {

		it('should save the first and the last routers the first time is called', () => {

			routerChain.addRouter(routerMock);

			expect((routerChain as any).first).toBe(routerMock);
			expect((routerChain as any).last).toBe(routerMock);
		});

		it('should save the next router in the last router the consecutive times that is called', () => {

			const router: AbstractResourceRouter = instance(routerMock);
			const router2: AbstractResourceRouter = instance(router2Mock);

			routerChain.addRouter(router);
			routerChain.addRouter(router2);

			expect((routerChain as any).first).toBe(router);
			expect((routerChain as any).last).toBe(router2);
		});

		it('should save the next router in the last router the consecutive times that is called ', () => {

			const router: AbstractResourceRouter = instance(routerMock);
			const router2: AbstractResourceRouter = instance(router2Mock);
			const router3Mock = mock(AbstractResourceRouter);
			const router3: AbstractResourceRouter = instance(router3Mock);

			routerChain.addRouter(router);
			routerChain.addRouter(router2);
			routerChain.addRouter(router3);

			expect((routerChain as any).first).toBe(router);
			expect((routerChain as any).last).toBe(router3);
			expect((routerChain as any).first.next).toBe(router2);
		});
	});

	describe('the method "route"', () => {

		it('should throw and exception if there are no routers defined', () => {
			expect(() => routerChain.route(PATH)).toThrow();
		});

		it('should call the method "route" of the first router if it is defined', () => {

			when(routerMock.route(PATH)).thenReturn();
			const router: AbstractResourceRouter = instance(routerMock);

			routerChain.addRouter(router);
			routerChain.route(PATH);

			verify(routerMock.route(PATH)).once();
		});
	});
});
