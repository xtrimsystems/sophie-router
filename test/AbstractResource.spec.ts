import { mock } from 'ts-mockito';

import { AbstractApplication, AbstractResource } from '../src';

import { AbstractResourceFixture } from './fixtures/AbstractResourceFixture';

describe('Test AbstractResource class', () => {

	describe('the method "isIdentifiedBy"', () => {

		const abstractResourceFixture: AbstractResource = new AbstractResourceFixture();
		abstractResourceFixture.run(mock(AbstractApplication));

		it('should return true if the path given corresponds with the path it can handle', () => {

			expect(abstractResourceFixture.isIdentifiedBy('/')).toBe(true);
		});

		it('should return false if the path given does not corresponds with the path it can handle', () => {

			expect(abstractResourceFixture.isIdentifiedBy('/foo')).toBe(false);
		});
	});
});
