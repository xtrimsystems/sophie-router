module.exports = {
	"transform": {
		"^.+\\.ts$": "ts-jest"
	},
	"testRegex": "(^/test/.*|(\\.|/)(test|spec))\\.(jsx?|tsx?)$",
	"moduleFileExtensions": [
		"ts",
		"js"
	],
	"reporters": [
		"default",
		["jest-junit", {"output": "./coverage/test-reports/jest-junit.xml"}]
	]
};
